<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\City;
use App\Models\Country;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Country::factory(5)->has(City::factory()->has(User::factory()->count(2))->count(5))->create();
        User::factory()->create([
            'city_id' => City::first()['id'],
            'admin'   => true,
            'email'   => 'admin@example.com'
        ]);
    }
}
