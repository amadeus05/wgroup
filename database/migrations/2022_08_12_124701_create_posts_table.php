<?php

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('status')->default(Post::STATUS_PENDING);
            $table->text('title');
            $table->text('description');
            $table->text('image')->nullable();
            $table->char('visibility_type')->default(Post::VISIBILITY_PUBLIC);
            $table->text('reject_message')->nullable();
            $table->foreignIdFor(User::class)->constrained();
            $table->timestamps();

            $table->index(['status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
