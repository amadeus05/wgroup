@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<div class="container-fluid">
    All posts
    <ul class="list-group">
        @foreach($posts as $post)
        <div class="row">

            <li class="list-group-item"><a href={{URL::to("/posts/$post->id")}}>TITLE: {{$post->title}}</a></li>
        </div>
        @endforeach

    </ul>

</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');

</script>
@stop
