@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Posts</h1>
@stop

@section('content')
<div class="container-fluid">
    {{-- {{dd($post)}} --}}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create post</h3>
                </div>
                <form method="POST" action="{{ route('posts.update', [$post->id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="card-body">
                        @if(auth()?->user()->admin == 1)
                        <div class="form-group">
                            <label>status</label>
                            <select name="status" class="custom-select">
                                <option @selected(old('status', $post->status) === "pending") value="pending">pending</option>
                                <option @selected(old('status', $post->status) === "rejected") value="rejected">rejected</option>
                                <option @selected(old('status', $post->status) === "approved") value="approved">approved</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Reject message</label>
                            <textarea name="reject_message" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ old('reject_message', $post->reject_message) }}</textarea>
                        </div>

                        @endif

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input value="{{ old('title', $post->title) }}" name="title" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter title">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Contents</label>
                            <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ old('description', $post->description) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="image" type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose image</label>
                                </div>
                                <div class="input-group-append">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Visibility type</label>
                            <select name="visibility_type" class="custom-select">
                                <option @selected(old('visibility_type', $post->visibility_type) === "1") value="1">private</option>
                                <option @selected(old('visibility_type', $post->visibility_type) === "2") value="2">public</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button @disabled($post->status == "pending" && auth()->user()->admin == 0) type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
<script>
    console.log('Hi!');

</script>
@stop
