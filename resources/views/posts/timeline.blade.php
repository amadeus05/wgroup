<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<style>
    .wrp {
        padding: 10px;
        display: flex;
        flex-direction: column;
    }

    .logout {
        padding: 10px;
        border-radius: 10px;
        border: 1px solid blue;
        width: 150px;
        max-width: 350px;
        margin-left: auto;
        margin-bottom: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        text-decoration: none
    }

    .top__btns__wrapper {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

</style>

<body>
    <div class="container">
        <div class="wrp">
            @auth

            <div class="top__btns__wrapper">
                @if($messages_count > 0)
                <a style="margin-left: 10px" class="logout" href="{{ url("/posts") }}">
                    <i class="fa-solid fa-comment"></i>
                    <span style="margin-left: 10px">messages {{$messages_count}}</span>
                </a>
                @endif

                <a class="logout" href="{{ url("posts") }}">
                    <i class="fa-solid fa-arrow-right-from-bracket"></i>
                    <span style="margin-left: 10px">Admin panel</span>
                </a>

                <a class="logout" href="{{ url("logout") }}">
                    <i class="fa-solid fa-arrow-right-from-bracket"></i>
                    <span style="margin-left: 10px">logout</span>
                </a>
            </div>

            @endauth
            @guest
            <a class="logout" href="{{ url("login") }}">
                <i class="fa-solid fa-arrow-right-to-bracket"></i>
                <span style="margin-left: 10px">Login to see privat posts</span>
            </a>

            @endguest

            @foreach($posts as $post)

            <div class="well">
                <div class="media">
                    <a class="pull-left" href="#">
                        <img style="width: 150px" class="media-object" src="{{asset("storage/$post->image")}}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$post->title}}</h4>
                        <p class="text-right"> Autor: {{$post->user->name}}</p>
                        <a href="{{route('posts.show', [$post->id])}}">read more ...</a>
                        <ul class="list-inline list-unstyled">
                            <li><span><i class="glyphicon glyphicon-calendar"></i> {{$post->created_at}} </span></li>
                            <li>|</li>
                            <span><i class="glyphicon glyphicon-comment"></i> 2 comments</span>
                            <li>|</li>
                            <li>

                                @php
                                $stars = rand(1,5);
                                $empty_stars = 5 - $stars;
                                @endphp

                                @for($i = 0; $i < $stars; $i++) <span class="glyphicon glyphicon-star"></span>
                                    @endfor

                                    @for($i = 0; $i < $empty_stars; $i++) <span class="glyphicon glyphicon-star-empty"></span>
                                        @endfor

                            </li>
                            <li>|</li>
                            <li>
                                {{$post->visibility_type == 1 ? 'private post' : 'public post' }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</body>

</html>
