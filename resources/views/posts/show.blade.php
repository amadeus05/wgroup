@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">POST ID {{$post->id}}</h3>
                    <div class="card-tools">
                    </div>
                </div>

                <div class="card-body p-0">
                    <div class="mailbox-read-info">
                        <h5>Post type: {{$post->visibility_type == 1 ? 'private' : 'public'}} </h5>
                        <h6>From: {{$post->user->name}}
                            <span class="mailbox-read-time float-right">{{$post->created_at}}</span></h6>
                    </div>

                    <div class="mailbox-read-message">
                        <p>{{$post->description}}</p>
                    </div>

                </div>

                @if($post->image)
                <div class="card-footer bg-white">
                    <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                        <li>
                            <span class="mailbox-attachment-icon has-img"><img src="{{ asset("storage/$post->image") }}" alt="Attachment"></span>
                            <div class="mailbox-attachment-info">
                            </div>
                        </li>
                    </ul>
                </div>
                @endif

            </div>

        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');

</script>
@stop
