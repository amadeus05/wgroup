@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Posts</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create post</h3>
                </div>
                <form id="xxx" method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="title" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter title">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Contents</label>
                            <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="image" type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose image</label>
                                </div>
                                <div class="input-group-append">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Visibility type</label>
                            <select name="visibility_type" class="custom-select">
                                <option value="2">public</option>
                                <option value="1">private</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
<script>
    console.log('Hi!');

</script>
@stop








{{-- <div class="row">
        <div class="col-md-6">

            <div class="card card-primary">


            <form class="form-horizontal" role="form" action="{{ route('posts.store') }}">

{{ method_field('POST') }}

{!! csrf_field() !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-md-2 control-label">Title</label>

    <div class="col-md-6">
        <input type="text" class="form-control" name="title" value="{{ old('title') }}">

        @if ($errors->has('title'))
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-md-2 control-label">Contents</label>

    <div class="col-md-6">
        <textarea class="form-control" name="description">{!! htmlentities(old('description')) !!}</textarea>

        @if ($errors->has('description'))
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="mb-5">
    <label for="Image" class="form-label">Bootstrap 5 image Upload with Preview</label>
    <input name="image" class="form-control" type="file" id="formFile" onchange="preview()">
</div>
<img id="frame" src="" class="img-fluid" />


<div class="form-group">
    <div class="col-md-6 col-md-offset-2">
        <button type="submit" class="btn btn-primary">
            Save
        </button>
    </div>
</div>

</form>

</div>

</div> --}}
