@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Striped Full Width Table</h3>
        </div>

        <div class="card-body p-0">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        {{-- @if(auth()?->user()->admin == 1) --}}
                        <th>STATUS</th>
                        {{-- @endif --}}
                        <th>VISIBILITY</th>
                        <th style="width: 40px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    <tr>

                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        {{-- @if(auth()?->user()->admin == 1) --}}
                        <th>{{$post->status}}</th>
                        {{-- @endif --}}
                        <td>{{$post->visibility_type == 2 ? 'public' : 'private'}}</td>
                        <td>
                            <div class="margin">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">

                                        <a class=" dropdown-item" href="{{route('posts.edit', [$post->id])}}">edit</a>
                                        <form class=" dropdown-item" method="post" action="{{"posts/" . $post->id}}">
                                            @csrf
                                            @method('DELETE')
                                            <button style=" border: none; background: none; padding: 0;" type="submit">delete</button>
                                        </form>
                                        <form class=" dropdown-item" method="post" action="{{route('posts.publish', [$post->id])}}">
                                            @csrf
                                            <button style=" border: none; background: none; padding: 0;" type="submit">publish</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>


</div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');

    function preview() {
        frame.src = URL.createObjectURL(event.target.files[0]);
    }

</script>
@stop
{{-- function clearImage() {
        document.getElementById('formFile').value = null;
        frame.src = "";
    } --}}
