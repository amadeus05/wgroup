<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeoController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\MessageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::prefix('geo')->group(function () {
//     Route::prefix('countries')->group(function () {
//         Route::get('', [GeoController::class, 'indexCountries'])->name('geo.index.countries');
//         Route::get('{country_id}/cities', [GeoController::class, 'indexCitiesByCounty'])->name('geo.index.cities.by.country');
//     });
// });

// Route::prefix('auth')->group(function () {
//     Route::post('registration', [AuthController::class, 'registration'])->name('registration');
//     Route::post('login', [AuthController::class, 'login'])->name('login');
// });

// Route::middleware('auth:sanctum')->group(function () {
//     Route::post('auth/logout', [AuthController::class, 'logout'])->name('logout');
//     Route::prefix('messages')->group(function () {
//         Route::post('', [MessageController::class, 'store'])->name('messages.store');
//         Route::get('received', [MessageController::class, 'getUserReceivedMessages'])->name('messages.received');
//     });
//     Route::prefix('posts')->group(function () {
//         Route::post('', [PostController::class, 'store'])->name('messages.store');
//         Route::post('{post_id}/moderation', [PostController::class, 'moderation'])->name('posts.moderation');
//         Route::patch('{post_id}', [PostController::class, 'update'])->name('messages.update');
//     });
// });

// Route::get('posts', [PostController::class, 'index'])->name('posts.index');
