<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\WebAuthController;
use App\Http\Controllers\Admin\WebPostController;
use App\Http\Controllers\Admin\WebMessageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebPostController::class, 'timeline']);

Route::get('/register', [WebAuthController::class, 'registrationView']);
Route::get('/login', [WebAuthController::class, 'loginView'])->name('login');

Route::post('/register', [WebAuthController::class, 'register'])->name('web.admin.register');
Route::post('/login', [WebAuthController::class, 'login'])->name('web.admin.login');
Route::get('logout', [WebAuthController::class, 'logout'])->name('web.admin.logout')->middleware('auth');
Route::get('switch-role', [WebAuthController::class, 'switchRole'])->middleware('auth');

Route::prefix('posts')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::post('',         [WebPostController::class, 'store'])->name('posts.store');
        Route::get('create',    [WebPostController::class, 'create'])->name('posts.create')->middleware('panel-data');
        Route::get('{id}/edit', [WebPostController::class, 'edit'])->name('posts.edit')->middleware('panel-data');
        Route::patch('{id}',    [WebPostController::class, 'update'])->name('posts.update');
        Route::delete('{id}',   [WebPostController::class, 'delete'])->name('posts.delete');
        Route::get('',          [WebPostController::class, 'index'])->name('posts.index')->middleware('panel-data');
        Route::post('{id}',     [WebPostController::class, 'publish'])->name('posts.publish');
    });

    Route::get('{id}', [WebPostController::class, 'show'])->name('posts.show');

    Route::prefix('{post_id}/messages')->group(function () {
        Route::post('', [WebMessageController::class, 'store'])->name('messages.store');
    });
});
