<?php

namespace App\Http\Services\Message;

use App\Models\Message;
use App\Models\Post;

class MessageService
{
    public function store(array $payload): Message
    {
        $message                 = new Message();
        $message->user_from_id   = auth()->id();
        $message->user_to_id     = Post::find($payload['id'])->user->id;
        $message->text           = $payload['text'];
        $message->post_id        = $payload['id'];
        $message->save();

        return $message;
    }

    public function countUnread(): int
    {
        return Message::whereUserToId(auth()->id())->unread()->count();
    }
}
