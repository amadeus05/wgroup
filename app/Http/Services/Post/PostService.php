<?php

namespace App\Http\Services\Post;

use Exception;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class PostService
{

    public function timeline()
    {
        return Post::query()
            ->whereStatus(Post::STATUS_APPROVED)
            ->when(
                !auth()->user(),
                fn ($q) => $q->public()
            )->get();
    }

    public function store(array $payload): Post
    {
        $post                  = new Post();
        $post->title           = $payload['title'];
        $post->description     = $payload['description'];
        $post->visibility_type = $payload['visibility_type'];
        $post->user_id         = auth()->id();

        $post->image = ($payload['image'] ?? null)
            ? (new PostImageProcessingService($payload['image']))->process()
            : null;

        $post->status = Post::STATUS_CREATED;

        if (Arr::exists($payload, 'publish')) {
            $post->status = Post::STATUS_PENDING;
        }

        $post->save();

        return $post;
    }

    public function update(int $postId, array $payload): Post
    {

        $post = Post::findOrFail($postId);

        $this->postCanBeUpdated($post);

        foreach ($payload as $key => $value) {
            if (in_array($key, (new Post)->getFillable())) {
                $post->{$key} = $value;
            }
        }

        $post->image  = ($payload['image'] ?? null)
            ? (new PostImageProcessingService($payload['image'], $post->image))->process()
            : null;

        $post->save();

        return $post;
    }

    public function moderate(array $payload): Post
    {
        $post = new Post();
        $post->status = $payload['status'] ?? null;
        $post->status = $payload['reject_message'] ?? null;
        $post->save();

        return $post;
    }

    public function postCanBeUpdated(int|Post $value): bool
    {

        if ((bool)auth()->user()->admin === true)
            return true;

        $post = $value;


        if (!$value instanceof Model) {
            $post = Post::findOrFail($value);
        }
        throw_if(
            ($post->status === Post::STATUS_PENDING || $post->status === Post::STATUS_APPROVED),
            new Exception('this post is not available for editing anymore', 422)
        );

        return true;
    }
}
