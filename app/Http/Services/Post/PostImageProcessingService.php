<?php

namespace App\Http\Services\Post;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PostImageProcessingService
{
    public function __construct(private UploadedFile $file, private ?string $oldImagePath = null)
    {
    }

    public function process()
    {
        if ($this->oldImagePath)
            Storage::disk('public')->delete($this->oldImagePath);

        return Storage::disk('public')->put('posts/images', $this->file);
    }
}
