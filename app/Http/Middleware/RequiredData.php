<?php

namespace App\Http\Middleware;

use App\Http\Services\Message\MessageService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RequiredData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        Session::forget('count_messages');
        Session::push('count_messages', (new MessageService)->countUnread());
        return $next($request);
    }
}
