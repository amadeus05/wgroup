<?php

namespace App\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email'    => ['email', 'required', 'unique:users,email'],
            'password' => ['string', 'required'],
            'name'     => ['required', 'string', 'min:4'],
            'birthday' => ['required', 'date_format:Y-m-d'],
            'sex'      => ['required', 'in:' . User::SEX_MAN . ',' . User::SEX_WOMAN],
            'city_id'  => ['required', 'exists:cities,id'],
        ];
    }

    public function processing(): User
    {
        $user           = new User();
        $user->email    = $this->email;
        $user->password = Hash::make($this->password);
        $user->name     = $this->name;
        $user->birthday = $this->birthday;
        $user->sex      = $this->sex;
        $user->city_id  = $this->city_id;
        $user->save();

        Auth::login($user);

        return $user;
    }
}
