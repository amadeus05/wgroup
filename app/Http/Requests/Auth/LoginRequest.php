<?php

namespace App\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email'    => ['email', 'required', 'exists:users,email'],
            'password' => ['string', 'required'],
        ];
    }

    public function processing()
    {
        $user = User::email($this->email)->first();

        if (!$user) {
            throw ValidationException::withMessages([
                'login_params' => [trans('auth.failed')],
            ]);
        }

        if (!Hash::check($this->password, $user->password)) {
            throw ValidationException::withMessages([
                'login_params' => [trans('auth.failed')],
            ]);
        }

        Auth::login($user);

        return [
            'token'      => $user->createToken('access_token')->plainTextToken,
            'token_type' => 'Bearer'
        ];
    }
}
