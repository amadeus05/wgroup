<?php

namespace App\Http\Requests\Web\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class PublishPostRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = Post::findOrFail($this?->id);
        return (int)$post->user_id === auth()->id();
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->route('id'),
        ]);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:posts,id'
        ];
    }

    public function processing()
    {
        $post = Post::find($this->id);
        $post->status = Post::STATUS_PENDING;
        $post->save();
    }
}
