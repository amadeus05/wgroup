<?php

namespace App\Http\Requests\Web\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = Post::findOrFail($this->id);
        $this->post = $post;
        if ($post->user_id === auth()->id() || (bool)auth()->user()->admin === true) {
            return true;
        }
        return false;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->route('id'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        $data = [
            'title'           => ['sometimes', 'string'],
            'description'     => ['sometimes', 'string'],
            'image'           => ['sometimes', 'image'],
            'visibility_type' => ['sometimes', 'in:1,2'],
            'publish'         => ['sometimes', 'in:1,2'],
            'id'              => ['required', 'exists:posts,id']
        ];

        if ((bool)auth()->user()->admin === true) {
            $data['reject_message'] = ['sometimes', 'string', 'nullable'];
            $data['status']         = ['sometimes', 'in:' . Post::STATUS_APPROVED . ',' . Post::STATUS_REJECTED . ',' . Post::STATUS_PENDING];
        }

        return $data;
    }
}
