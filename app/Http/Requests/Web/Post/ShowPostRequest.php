<?php

namespace App\Http\Requests\Web\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class ShowPostRequest extends FormRequest
{

    private Post $post;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = Post::findOrFail($this->id);
        $this->post = $post;
        if ($post->visibility_type === Post::VISIBILITY_PRIVATE) {
            return !auth()->user();
        }
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->route('id'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:posts,id'
        ];
    }

    public function getPost(): Post
    {
        return $this->post;
    }
}
