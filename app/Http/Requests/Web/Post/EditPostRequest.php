<?php

namespace App\Http\Requests\Web\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class EditPostRequest extends FormRequest
{
    private Post $post;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->post = Post::findOrFail($this->id);
        return (auth()->id() === $this->post->user_id || (bool)auth()->user()->admin === true);
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->route('id'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:posts,id'
        ];
    }

    public function getPost(): Post
    {
        return $this->post;
    }
}
