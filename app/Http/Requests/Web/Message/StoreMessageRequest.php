<?php

namespace App\Http\Requests\Web\Message;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class StoreMessageRequest extends FormRequest
{

    private Post $post;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = Post::findOrFail($this?->id);
        if ($post->visibility_type == Post::VISIBILITY_PRIVATE && !auth()->user()) {
            return false;
        }
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->route('post_id'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'      => 'required|exists:posts,id',
            'text'    => 'required|string|max:10000',
        ];
    }

    public function getPost(): Post
    {
        return $this->post;
    }
}
