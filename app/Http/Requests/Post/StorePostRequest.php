<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'           => ['required', 'string'],
            'description'     => ['required', 'string'],
            'image'           => ['sometimes', 'file', 'max:100000'],
            'users'           => ['array', 'sometimes'],
            'users.*'         => ['exists:users,id'],
            'visibility_type' => ['required', 'in:1,2'],
            'publish'         => ['sometimes', 'in:1,2'],
        ];
    }
}
