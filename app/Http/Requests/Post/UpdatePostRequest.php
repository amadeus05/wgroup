<?php

namespace App\Http\Requests\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'post_id' => $this->route('post_id'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validation = [
            'post_id'         => ['required', 'exists:posts,id'],
            'title'           => ['sometimes', 'string'],
            'description'     => ['sometimes', 'string'],
            'image'           => ['sometimes', 'file', 'max:100000'],
            'visibility_type' => ['sometimes', 'in:1,2'],
            'publish'         => ['sometimes', 'in:1,2'],
        ];

        if (auth()->user()->admin === 1) {
            $validation = [
                'reject_message'  => ['sometimes', 'string'],
                'status'          => ['sometimes', 'in:' . Post::STATUS_APPROVED . ',' . Post::STATUS_REJECTED],
            ];
        }

        return $validation;
    }
}
