<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Post\PostService;
use App\Http\Services\Message\MessageService;
use App\Http\Requests\Web\Post\EditPostRequest;
use App\Http\Requests\Web\Post\ShowPostRequest;
use App\Http\Requests\Web\Post\StorePostRequest;
use App\Http\Requests\Web\Post\CreatePostRequest;
use App\Http\Requests\Web\Post\DeletePostRequest;
use App\Http\Requests\Web\Post\PublishPostRequest;
use App\Http\Requests\Web\Post\UpdatePostRequest;
use App\Models\Message;
use App\Models\Post;
use App\Models\User;

class WebPostController extends Controller
{

    //==========================================VIEWS ROUTES SECTION===================================

    public function timeline(PostService $postService, MessageService $messageService)
    {
        return view('posts.timeline', [
            'posts'          => $postService->timeline(),
            'messages_count' => $messageService->countUnread()
        ]);
    }

    public function index()
    {
        $posts = Post::when(auth()?->user()->admin == 0, fn ($q) => $q->whereUserId(auth()->id()))->get();
        return view('posts.index', [
            'posts' => $posts,
        ]);
    }

    public function create(CreatePostRequest $request)
    {
        return view('posts.create');
    }

    public function show(int $postId, ShowPostRequest $request, MessageService $messageService)
    {
        return view('posts.timeline_post', [
            'post'     => $request->getPost(),
            'messages' => Message::whereUserToId(auth()->id())->wherePostId($postId)->orderByDesc('created_at')->get(),
        ]);
    }

    public function edit(int $postId, EditPostRequest $request)
    {
        return view('posts.edit', ['post' => $request->getPost()]);
    }

    //==========================================VIEWS ROUTES END SECTION===================================

    public function update(int $postId, UpdatePostRequest $request, PostService $postService)
    {
        $postService->update($postId, $request->validated());
        return response()->redirectToRoute('posts.index');
    }

    public function store(StorePostRequest $request, PostService $postService)
    {
        $postService->store($request->validated());
        return response()->redirectToRoute('posts.index');
    }

    public function delete(int $postId, DeletePostRequest $request)
    {
        return response()->redirectTo(route($request->processing()));
    }

    public function publish(int $postId, PublishPostRequest $request)
    {
        $request->processing();
        return response()->redirectTo('/');
    }
}
