<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Message\MessageService;
use App\Http\Requests\Web\Message\StoreMessageRequest;
use App\Models\Message;

class WebMessageController extends Controller
{
    public function store(StoreMessageRequest $request, MessageService $messageService)
    {
        $messageService->store($request->validated());
        return response()->redirectToRoute('posts.show', [$request->id]);
    }

    public function unreadCount()
    {
        return response()->json([
            'count' => Message::whereUserToId(auth()->id())->unread()->count()
        ]);
    }
}
