<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\User;

class WebAuthController extends Controller
{
    public function loginView()
    {
        return view('vendor.adminlte.auth.login');
    }

    public function registrationView()
    {
        return view('vendor.adminlte.auth.register', ['cities' => City::get()]);
    }

    public function register(RegistrationRequest $request)
    {
        $request->processing();
        return response()->redirectTo('/');
    }

    public function login(LoginRequest $request)
    {
        $request->processing();
        return response()->redirectTo('/');
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('login');
    }

    public function switchRole()
    {
        $user = User::find(auth()->id());
        $user->admin = $user->admin == 1 ? 0 : 1;
        $user->save();
        return response()->redirectTo('/posts');
    }
}
