<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'user_from_id',
        'user_to_id',
        'post_id',
        'read'
    ];

    protected $casts = [
        'text'         => 'string',
        'user_from_id' => 'integer',
        'user_to_id'   => 'integer',
        'post_id'      => 'integer',
        'read'         => 'boolean',
    ];

    public function sended(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_to_id');
    }

    public function received(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_from_id');
    }

    public function post(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_from_id');
    }

    public function scopeUnread($q)
    {
        $q->where('read', false);
    }
}
