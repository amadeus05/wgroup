<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Post extends Model
{
    use HasFactory;

    const STATUS_APPROVED   = 'approved';
    const STATUS_REJECTED   = 'rejected';
    const STATUS_PENDING    = 'pending';
    const STATUS_CREATED    = 'created';

    const VISIBILITY_PRIVATE = 1;
    const VISIBILITY_PUBLIC  = 2;

    protected $fillable = [
        'title',
        'description',
        'user_id',
        'status',
        'image',
        'visibility_type',
        'reject_message',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePrivate($q)
    {
        $q->where('visibility_type', self::VISIBILITY_PRIVATE);
    }

    public function scopePublic($q)
    {
        $q->where('visibility_type', self::VISIBILITY_PUBLIC);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'user_posts', 'post_id', 'user_id');
    }
}
